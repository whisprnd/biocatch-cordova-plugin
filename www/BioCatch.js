const exec = require('cordova/exec');

const PLUGIN_NAME = 'BioCatch';

module.exports = {
  start: (customerSessionID, wupUrl, publicKey, onSuccess, onError) => {
    exec(onSuccess, onError, PLUGIN_NAME, 'start', [customerSessionID, wupUrl, publicKey]);
  },

  pause: (success, error) => {
    exec(success, error, PLUGIN_NAME, "pause", [])
  },

  resume: (success, error) => {
    exec(success, error, PLUGIN_NAME, "resume", [])
  },

  stop: (success, error) => {
    exec(success, error, PLUGIN_NAME, "stop", [])
  },

  resetSession: (success, error) => {
    exec(success, error, PLUGIN_NAME, "resetSession", [])
  },

  changeContext: (contextName, success, error) => {
    exec(success, error, PLUGIN_NAME, "changeContext", [contextName])
  },

  updateCustomerSessionID: (customerSessionID, success, error) => {
    exec(success, error, PLUGIN_NAME, "updateCustomerSessionID", [customerSessionID])
  },
};

